package com.weatherapp.ui.bookmark;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.weatherapp.R;
import com.weatherapp.database.DatabaseHandler;
import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;
import com.weatherapp.ui.MainActivity;
import com.weatherapp.ui.home.HomeFragment;
import com.weatherapp.utils.ActivityUtils;
import com.weatherapp.web.WeatherUtil;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

class BookmarkPresenter implements BookmarkContract.Presenter,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private final BookmarkContract.View bookmarkView;
    private GoogleApiClient mGoogleApiClient;
    private CameraPosition currentMarkerPosition;

    BookmarkPresenter(BookmarkContract.View bookmarkView) {
        this.bookmarkView = bookmarkView;
    }

    @Override
    public GoogleApiClient buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(bookmarkView.getParentActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        return null;
    }

    @Override
    public void releaseApiClient() {
        mGoogleApiClient.disconnect();
    }

    @Override
    public void addLocationToBookmark() {
        if(currentMarkerPosition !=null){
            Location location = new Location();
            location.setLatitude(currentMarkerPosition.target.latitude);
            location.setLongitude(currentMarkerPosition.target.longitude);
            fetchWeatherForLocation(location);
        } else {
            Toast.makeText(bookmarkView.getParentActivity(),"Location not available, Please wait...",Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchWeatherForLocation(final Location location) {
        bookmarkView.showProgressDialog("Fetching Data...");
        WeatherUtil weatherUtil = new WeatherUtil(bookmarkView.getParentActivity(), new WeatherUtil.WeatherUpdateListener() {
            @Override
            public void onWeatherFetched(LocationWeather locationWeather) {
                bookmarkView.hideProgressDialog();
                saveLocationInDB(location, locationWeather);
            }

            @Override
            public void onFutureWeatherFetched(List<LocationWeather> locationWeathers) {
                //do nothing
            }

            @Override
            public void onWeatherFailed(String message) {
                bookmarkView.hideProgressDialog();
                Toast.makeText(bookmarkView.getParentActivity(),message,Toast.LENGTH_SHORT).show();
            }
        });
        weatherUtil.fetchWeatherForLocation(location);
    }

    private void saveLocationInDB(Location location, LocationWeather locationWeather) {
        MainActivity parentActivity = (MainActivity) bookmarkView.getParentActivity();
        DatabaseHandler dbHelper = parentActivity.getDBHelper();
        Geocoder geocoder = new Geocoder(bookmarkView.getParentActivity(), Locale.getDefault());

        try {
            List<Address> fromLocation = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (fromLocation.size() > 0)
                location.setName(fromLocation.get(0).getLocality());
            else
                location.setName("Name Not Available");

            int locationId = dbHelper.insertLocation(location);
            location.setId(locationId);
            saveWeatherInDB(dbHelper, location, locationWeather);

            handler.sendEmptyMessage(NAV_HOME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final int NAV_HOME = 1;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == NAV_HOME) {
                ActivityUtils.addFragment(bookmarkView.getParentActivity(), new HomeFragment(), R.id.container);
            }
            super.handleMessage(msg);
        }
    };


    private void saveWeatherInDB(DatabaseHandler dbHelper, Location location, LocationWeather locationWeather) throws IOException {
        Gson gson = new Gson();
        locationWeather.setLocation(location);
        locationWeather.setWeatherJson(gson.toJson(locationWeather));
        dbHelper.insertWeather(locationWeather);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(bookmarkView.getParentActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
                @Override
                public void onLocationChanged(android.location.Location location) {
                    //do nothing.
                }
            });
            android.location.Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(lastLocation != null){
                addMarkerToMap(lastLocation);
            }
        }
    }

    private void addMarkerToMap(android.location.Location lastLocation) {
        final GoogleMap googleMapView = bookmarkView.getGoogleMapView();
        LatLng coordinate = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        googleMapView.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 10f));
        googleMapView.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                currentMarkerPosition = googleMapView.getCameraPosition();
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("onConnectionFailed", "onConnectionFailed");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("onConnectionFailed", connectionResult.toString());
    }
}
