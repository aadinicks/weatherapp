package com.weatherapp.ui.bookmark;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.weatherapp.ui.BaseView;

public interface BookmarkContract {

    interface View extends BaseView{
        GoogleMap getGoogleMapView();

        void showProgressDialog(String title);

        void hideProgressDialog();

        void removeSelf();
    }

    interface Presenter {
        GoogleApiClient buildGoogleApiClient();

        void releaseApiClient();

        void addLocationToBookmark();
    }
}
