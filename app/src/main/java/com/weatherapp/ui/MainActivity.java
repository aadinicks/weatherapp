package com.weatherapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.weatherapp.R;
import com.weatherapp.database.DatabaseHandler;
import com.weatherapp.ui.home.HomeFragment;
import com.weatherapp.utils.ActivityUtils;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    ProgressBar progressBar;
    FrameLayout secondaryContainer;

    private DatabaseHandler databaseHandler;
    private HomeFragment homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        secondaryContainer = (FrameLayout) findViewById(R.id.secondary_container);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar) findViewById(R.id.toolbar_progress_bar);

        setSupportActionBar(toolbar);
        homeFragment = new HomeFragment();
        ActivityUtils.addFragment(this, homeFragment, R.id.container);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        Fragment fragment;
//        if (isLandscapeMode()) {
//            fragment = getSupportFragmentManager().findFragmentById(R.id.secondary_container);
//        } else {
//            fragment = getSupportFragmentManager().findFragmentById(R.id.container);
//        }
//        if(fragment!=null)
//            outState.putString("current", fragment.getClass().getName());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
//        if (savedInstanceState != null) {
//            String currentFragmentTag = savedInstanceState.getString("current");
//            Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
//            if(fragmentByTag!=null){
//                if (isLandscapeMode()) {
//                    ActivityUtils.addFragment(this, fragmentByTag, R.id.secondary_container);
//                } else {
//                    ActivityUtils.addFragment(this, fragmentByTag, R.id.container);
//                }
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment instanceof HomeFragment) {
            finish();
        } else {
            getSupportFragmentManager().popBackStackImmediate(null, 0);
        }
    }

    public DatabaseHandler getDBHelper() {
        if (databaseHandler == null) {
            databaseHandler = new DatabaseHandler(this);
        }
        return databaseHandler;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (databaseHandler != null) {
            databaseHandler = null;
        }
    }

    public void showToolbarProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideToolbarProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public boolean isLandscapeMode() {
        return secondaryContainer != null;
    }
}
