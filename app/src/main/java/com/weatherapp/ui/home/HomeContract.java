package com.weatherapp.ui.home;

import com.weatherapp.database.models.Location;
import com.weatherapp.ui.BaseView;

import java.util.List;

public interface HomeContract {

    interface View extends BaseView {
        void onCityFetched(List<Location> cities);

        void onLocationSelected(Location location);

        void onAddLocationSelected();
    }

    interface Presenter {
        void fetchBookmarkedCities();
    }
}
