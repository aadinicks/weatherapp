package com.weatherapp.ui.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.weatherapp.R;
import com.weatherapp.adapters.CityRecyclerAdapter;
import com.weatherapp.adapters.RecyclerViewItemClickListener;
import com.weatherapp.database.DatabaseHandler;
import com.weatherapp.database.models.Location;
import com.weatherapp.ui.HelpFragment;
import com.weatherapp.ui.MainActivity;
import com.weatherapp.ui.bookmark.AddLocationFragment;
import com.weatherapp.ui.detail.DetailFragment;
import com.weatherapp.ui.settings.SettingsFragment;
import com.weatherapp.utils.ActivityUtils;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements HomeContract.View, RecyclerViewItemClickListener {

    RecyclerView recyclerView;

    private HomeContract.Presenter homePresenter;
    private CityRecyclerAdapter cityRecyclerAdapter;
    private List<Location> locations;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.help:
                ActivityUtils.addFragment(getParentActivity(), new HelpFragment(), R.id.container);
                break;
            case R.id.settings:
                ActivityUtils.addFragment(getParentActivity(), new SettingsFragment(), R.id.container);
                break;
        }
        return true;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        locations = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = (RecyclerView) inflatedView.findViewById(R.id.locations_list);
        getActivity().setTitle("My Locations");
        return inflatedView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        homePresenter = new HomePresenter(this);
        homePresenter.fetchBookmarkedCities();
    }

    @Override
    public void onCityFetched(List<Location> locations) {
        this.locations = locations;

        cityRecyclerAdapter = new CityRecyclerAdapter(this.locations);
        cityRecyclerAdapter.setItemClickListener(this);
        recyclerView.setAdapter(cityRecyclerAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onItemClicked(View view, int position) {
        if (position == locations.size()) {
            onAddLocationSelected();
        } else {
            onLocationSelected(locations.get(position));
        }
    }

    @Override
    public void onAddLocationSelected() {
        AddLocationFragment addLocationFragment = new AddLocationFragment();
        if (((MainActivity) getParentActivity()).isLandscapeMode()) {
            ActivityUtils.addFragment(getParentActivity(), addLocationFragment, R.id.secondary_container);
        } else {
            ActivityUtils.addFragment(getParentActivity(), addLocationFragment, R.id.container);
        }
    }

    @Override
    public void onLocationSelected(Location location) {
        DetailFragment detailFragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("location", location);
        detailFragment.setArguments(bundle);
        if (((MainActivity) getParentActivity()).isLandscapeMode()) {
            ActivityUtils.addFragment(getParentActivity(), detailFragment, R.id.secondary_container);
        } else {
            ActivityUtils.addFragment(getParentActivity(), detailFragment, R.id.container);
        }
    }

    @Override
    public void onItemLongClicked(View view, int position) {
        if (position != locations.size()) {
            Location location = locations.get(position);
            DatabaseHandler dbHelper = ((MainActivity) getActivity()).getDBHelper();
            dbHelper.deleteLocation(location);
            locations.remove(position);
            cityRecyclerAdapter.notifyDataSetChanged();
            Toast.makeText(getActivity(), "Location Removed from Bookmarks!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public FragmentActivity getParentActivity() {
        return getActivity();
    }
}
