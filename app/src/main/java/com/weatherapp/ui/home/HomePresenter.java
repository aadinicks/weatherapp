package com.weatherapp.ui.home;

import com.weatherapp.database.DatabaseHandler;
import com.weatherapp.database.models.Location;
import com.weatherapp.ui.MainActivity;

import java.util.List;

class HomePresenter implements HomeContract.Presenter {
    private HomeContract.View homeView;

    HomePresenter(HomeContract.View homeView) {
        this.homeView = homeView;
    }

    @Override
    public void fetchBookmarkedCities() {
        DatabaseHandler dbHelper = ((MainActivity) homeView.getParentActivity()).getDBHelper();
        List<Location> locations = dbHelper.getAllLocations();
        homeView.onCityFetched(locations);
    }
}
