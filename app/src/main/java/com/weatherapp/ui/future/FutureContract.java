package com.weatherapp.ui.future;

import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;
import com.weatherapp.ui.BaseView;

import java.util.List;

public interface FutureContract {

    interface View extends BaseView {

        void updateWeather(List<LocationWeather> locationWeathers);

        void hideToolbarProgress();

        void showToolbarProgress();
    }

    interface Presenter {
        void fetchFutureWeatherForLocation(final Location location);
    }
}
