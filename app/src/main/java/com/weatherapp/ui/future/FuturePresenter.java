package com.weatherapp.ui.future;

import android.widget.Toast;

import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;
import com.weatherapp.web.WeatherUtil;

import java.util.List;

class FuturePresenter implements FutureContract.Presenter {

    private final FutureContract.View futureView;

    FuturePresenter(FutureContract.View futureView) {
        this.futureView = futureView;
    }

    @Override
    public void fetchFutureWeatherForLocation(final Location location) {
        futureView.showToolbarProgress();
        WeatherUtil weatherUtil = new WeatherUtil(futureView.getParentActivity(), new WeatherUtil.WeatherUpdateListener() {
            @Override
            public void onWeatherFetched(LocationWeather locationWeather) {
                //do nothing.
            }

            @Override
            public void onFutureWeatherFetched(List<LocationWeather> locationWeathers) {
                futureView.hideToolbarProgress();
                futureView.updateWeather(locationWeathers);
            }

            @Override
            public void onWeatherFailed(String message) {
                futureView.hideToolbarProgress();
                Toast.makeText(futureView.getParentActivity(),message,Toast.LENGTH_SHORT).show();
            }
        });
        weatherUtil.setFutureForecast(true);
        weatherUtil.fetchWeatherForLocation(location);
    }
}
