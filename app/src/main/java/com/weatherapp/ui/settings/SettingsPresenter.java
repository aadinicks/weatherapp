package com.weatherapp.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.weatherapp.R;
import com.weatherapp.database.DatabaseHandler;
import com.weatherapp.ui.MainActivity;
import com.weatherapp.ui.home.HomeFragment;
import com.weatherapp.utils.ActivityUtils;
import com.weatherapp.utils.Constants;

class SettingsPresenter implements SettingContract.Presenter {

    private final SharedPreferences sharedPreferences;
    SettingContract.View settingsView;

    public SettingsPresenter(SettingContract.View settingsView) {
        this.settingsView = settingsView;
        sharedPreferences = settingsView.getParentActivity().getSharedPreferences(Constants.SP_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void storeUnits(String selectedUnit) {
        sharedPreferences.edit().putString(Constants.SP_UNIT, selectedUnit).commit();
    }

    @Override
    public String getSelectedUnit() {
        return sharedPreferences.getString(Constants.SP_UNIT, "metric");
    }

    @Override
    public void resetBookmarks() {
        DatabaseHandler dbHelper = ((MainActivity) settingsView.getParentActivity()).getDBHelper();
        dbHelper.deleteAllLocations();
        Toast.makeText(settingsView.getParentActivity(), "All Bookmarks Removed!", Toast.LENGTH_SHORT).show();
        ActivityUtils.addFragment(settingsView.getParentActivity(), new HomeFragment(), R.id.container);
    }
}
