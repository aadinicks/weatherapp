package com.weatherapp.ui.settings;

import com.weatherapp.ui.BaseView;

public interface SettingContract {

    interface View extends BaseView {

    }

    interface Presenter {
        void storeUnits(String selectedUnit);

        String getSelectedUnit();

        void resetBookmarks();
    }

}
