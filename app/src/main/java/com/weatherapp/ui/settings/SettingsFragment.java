package com.weatherapp.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.weatherapp.R;


public class SettingsFragment extends Fragment implements SettingContract.View, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Spinner spnUnits;
    private Button btnReset;
    private SettingsPresenter settingsPresenter;
    private String[] unitsArr;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        spnUnits = (Spinner) view.findViewById(R.id.spn_units);
        btnReset = (Button) view.findViewById(R.id.btn_reset);
        spnUnits.setOnItemSelectedListener(this);
        btnReset.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        settingsPresenter = new SettingsPresenter(this);
        unitsArr = getResources().getStringArray(R.array.units);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getParentActivity(), android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.units));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnUnits.setAdapter(dataAdapter);
        spnUnits.setSelection(dataAdapter.getPosition(settingsPresenter.getSelectedUnit()));
    }

    @Override
    public FragmentActivity getParentActivity() {
        return getActivity();
    }

    @Override
    public void onClick(View v) {
        settingsPresenter.resetBookmarks();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        settingsPresenter.storeUnits(unitsArr[position]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
