package com.weatherapp.ui.detail;

import com.weatherapp.database.DatabaseHandler;
import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;
import com.weatherapp.ui.BaseView;

public interface DetailContract {

    interface View extends BaseView {

        void updateWeather(LocationWeather locationWeather);

        void hideToolbarProgress();

        void showToolbarProgress();

    }

    interface Presenter {
        void saveWeatherInDB(DatabaseHandler dbHelper, Location location, LocationWeather locationWeather);

        void fetchWeatherForLocation(final Location location);

        void fetchCachedWeather(Location location);
    }
}
