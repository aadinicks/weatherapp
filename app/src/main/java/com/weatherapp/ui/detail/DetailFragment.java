package com.weatherapp.ui.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.weatherapp.R;
import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;
import com.weatherapp.ui.MainActivity;
import com.weatherapp.ui.future.FutureFragment;
import com.weatherapp.utils.ActivityUtils;

public class DetailFragment extends Fragment implements DetailContract.View {

    TextView forecastView;
    TextView tempView;
    TextView humidityView;
    TextView windView;

    private Location location;
    private DetailPresenter detailPresenter;
    private TextView lbl_min_temp;
    private TextView lbl_max_temp;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.future:
                FutureFragment futureFragment = new FutureFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("location", location);
                futureFragment.setArguments(bundle);
                ActivityUtils.addFragment(getParentActivity(), futureFragment, R.id.container);
                break;
        }
        return true;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        Bundle arguments = getArguments();
        location = (Location) arguments.getSerializable("location");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.weather, container, false);

        forecastView = (TextView) inflatedView.findViewById(R.id.lbl_forecast);
        lbl_min_temp = (TextView) inflatedView.findViewById(R.id.lbl_min_temp);
        lbl_max_temp = (TextView) inflatedView.findViewById(R.id.lbl_max_temp);
        tempView = (TextView) inflatedView.findViewById(R.id.lbl_temp);
        humidityView = (TextView) inflatedView.findViewById(R.id.lbl_humidity);
        windView = (TextView) inflatedView.findViewById(R.id.lbl_wind);

        return inflatedView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(location.getName() == null ? "N.A" : location.getName());
        detailPresenter = new DetailPresenter(this);
        detailPresenter.fetchWeatherForLocation(location);
        detailPresenter.fetchCachedWeather(location);
    }

    @Override
    public void updateWeather(LocationWeather weather) {
        String weatherJson = weather.getWeatherJson();
        Gson gson = new Gson();
        LocationWeather locationWeather = gson.fromJson(weatherJson, LocationWeather.class);

        forecastView.setText(locationWeather.getWeather().get(0).getDescription());
        tempView.setText(String.valueOf(locationWeather.getMain().getTemp()) + (char) 0x00B0);
        lbl_max_temp.setText(String.valueOf(locationWeather.getMain().getTemp_max()) + (char) 0x00B0);
        lbl_min_temp.setText(String.valueOf(locationWeather.getMain().getTemp_min()) + (char) 0x00B0);
        humidityView.setText("Humidity: " + locationWeather.getMain().getHumidity() + " %");
        windView.setText("Wind: " + locationWeather.getWind().getSpeed() + " kmph");
    }

    @Override
    public FragmentActivity getParentActivity() {
        return getActivity();
    }


    @Override
    public void hideToolbarProgress() {
        ((MainActivity) getParentActivity()).hideToolbarProgress();
    }

    @Override
    public void showToolbarProgress() {
        ((MainActivity) getParentActivity()).showToolbarProgress();
    }

}
