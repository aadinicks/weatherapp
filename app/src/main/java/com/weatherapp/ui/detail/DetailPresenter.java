package com.weatherapp.ui.detail;

import android.widget.Toast;

import com.google.gson.Gson;
import com.weatherapp.database.DatabaseHandler;
import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;
import com.weatherapp.ui.MainActivity;
import com.weatherapp.web.WeatherUtil;

import java.util.List;

class DetailPresenter implements DetailContract.Presenter {

    private final DetailContract.View detailView;

    DetailPresenter(DetailContract.View detailView) {
        this.detailView = detailView;
    }

    @Override
    public void saveWeatherInDB(DatabaseHandler dbHelper, Location location, LocationWeather locationWeather) {
        Gson gson = new Gson();
        locationWeather.setLocation(location);
        locationWeather.setWeatherJson(gson.toJson(locationWeather));
        dbHelper.insertWeather(locationWeather);
    }

    @Override
    public void fetchWeatherForLocation(final Location location) {
        detailView.showToolbarProgress();
        WeatherUtil weatherUtil = new WeatherUtil(detailView.getParentActivity(), new WeatherUtil.WeatherUpdateListener() {
            @Override
            public void onWeatherFetched(LocationWeather locationWeather) {
                detailView.hideToolbarProgress();
                DatabaseHandler dbHelper = ((MainActivity) detailView.getParentActivity()).getDBHelper();
                saveWeatherInDB(dbHelper, location, locationWeather);
            }

            @Override
            public void onFutureWeatherFetched(List<LocationWeather> locationWeathers) {
                //do nothing.
            }

            @Override
            public void onWeatherFailed(String message) {
                detailView.hideToolbarProgress();
                Toast.makeText(detailView.getParentActivity(),message,Toast.LENGTH_SHORT).show();
            }
        });
        weatherUtil.fetchWeatherForLocation(location);
    }

    @Override
    public void fetchCachedWeather(Location location) {
        DatabaseHandler dbHelper = ((MainActivity) detailView.getParentActivity()).getDBHelper();
        LocationWeather locationWeather = dbHelper.getWeatherFromTable(location.getId());
        detailView.updateWeather(locationWeather);
    }
}
