package com.weatherapp.ui;

import android.support.v4.app.FragmentActivity;

public interface BaseView {

    FragmentActivity getParentActivity();
}
