package com.weatherapp.database.models;

import java.io.Serializable;

public class Location implements Serializable {

    private int id;
    private double latitude;
    private double longitude;
    private String name;

    public Location() {
    }

    public Location(int id, String name, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    private LocationWeather weather;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocationWeather getWeather() {
        return weather;
    }

    public void setWeather(LocationWeather weather) {
        this.weather = weather;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
