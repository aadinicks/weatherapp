package com.weatherapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "db_weather";

    private static final String TABLE_LOCATIONS = "tb_locations";
    private static final String TABLE_LOCATION_WEATHER = "tb_location_weather";

    // Location Table Columns
    private static final String KEY_ID = "id";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_NAME = "name";

    private String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATIONS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_NAME + " TEXT,"
            + KEY_LATITUDE + " DOUBLE,"
            + KEY_LONGITUDE + " DOUBLE" + ")";

    // Weather Table Columns
    private static final String KEY_WEATHER_ID = "id";
    private static final String KEY_LOCATION_ID = "location_id";
    private static final String KEY_JSON = "weather_json";

    private String CREATE_WEATHER_TABLE = "CREATE TABLE " + TABLE_LOCATION_WEATHER + "("
            + KEY_WEATHER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_LOCATION_ID + " INTEGER,"
            + KEY_JSON + " TEXT,"
            + " FOREIGN KEY (" + KEY_LOCATION_ID + ") REFERENCES " + TABLE_LOCATIONS + " (" + KEY_ID + "));";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_LOCATION_TABLE);
        db.execSQL(CREATE_WEATHER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // handle upgrades here.
    }

    public void insertWeather(LocationWeather weather) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOCATION_ID, weather.getLocation().getId());
        values.put(KEY_JSON, weather.getWeatherJson());

        if (getWeatherFromTable(weather.getLocation().getId()) == null) {
            db.insert(TABLE_LOCATION_WEATHER, null, values);
        } else {
            db.update(TABLE_LOCATION_WEATHER, values, KEY_LOCATION_ID + " = ?",
                    new String[]{String.valueOf(weather.getLocation().getId())});
        }
        db.close();
    }

    public int insertLocation(Location location) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, location.getName());
        values.put(KEY_LATITUDE, location.getLatitude());
        values.put(KEY_LONGITUDE, location.getLongitude());

        long insert = db.insert(TABLE_LOCATIONS, null, values);
        db.close();
        return (int) insert;
    }

    public LocationWeather getWeatherFromTable(int locationId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_LOCATION_WEATHER, new String[]{KEY_WEATHER_ID,
                        KEY_LOCATION_ID, KEY_JSON}, KEY_LOCATION_ID + "=?",
                new String[]{String.valueOf(locationId)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        if (cursor != null && cursor.getCount() > 0) {
            LocationWeather weather = new LocationWeather(Integer.parseInt(cursor.getString(0)),
                    cursor.getInt(1), cursor.getString(2));
            cursor.close();
            return weather;
        } else {
            cursor.close();
            return null;
        }
    }

    public void deleteLocation(Location location) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LOCATIONS, KEY_ID + " = ?",
                new String[]{String.valueOf(location.getId())});
        db.delete(TABLE_LOCATION_WEATHER, KEY_LOCATION_ID + " = ?",
                new String[]{String.valueOf(location.getId())});
        db.close();
    }

    public List<Location> getAllLocations() {
        List<Location> locations = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LOCATIONS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Location contact = new Location();
                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setLatitude(cursor.getDouble(2));
                contact.setLongitude(cursor.getDouble(3));

                locations.add(contact);
            } while (cursor.moveToNext());
        }
        return locations;
    }

    public void deleteAllLocations() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LOCATIONS, null, null);
        db.delete(TABLE_LOCATION_WEATHER, null, null);
        db.close();
    }
}