package com.weatherapp.adapters;

import android.view.View;

public interface RecyclerViewItemClickListener {

    void onItemClicked(View view, int position);

    void onItemLongClicked(View view, int position);
}
