package com.weatherapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weatherapp.R;
import com.weatherapp.database.models.Location;
import com.weatherapp.utils.FontManager;

import java.util.List;

public class CityRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    public static final int ADD_CITY = 1;
    public static final int CITY = 0;

    private final List<Location> cities;
    private RecyclerViewItemClickListener itemClickListener;

    public CityRecyclerAdapter(List<Location> locationList) {
        this.cities = locationList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View inflatedView;
        switch (viewType) {
            case CITY:
                inflatedView = layoutInflater.inflate(R.layout.item_city, parent, false);
                return new CityViewHolder(inflatedView);
            case ADD_CITY:
                inflatedView = layoutInflater.inflate(R.layout.add_city, parent, false);
                return new AddCityViewHolder(inflatedView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case ADD_CITY:
                ((AddCityViewHolder) holder).layoutAdd.setTag(position);
                ((AddCityViewHolder) holder).layoutAdd.setOnClickListener(this);
                break;
            case CITY:
                Location location = cities.get(position);
                CityRecyclerAdapter.CityViewHolder cityViewHolder = (CityViewHolder) holder;
                cityViewHolder.lblCityName.setText(location.getName() == null ? "N.A." : location.getName());
                cityViewHolder.layoutCity.setTag(position);
                cityViewHolder.layoutCity.setOnClickListener(this);
                cityViewHolder.layoutCity.setOnLongClickListener(this);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return cities.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == cities.size()) {
            return ADD_CITY;
        } else {
            return CITY;
        }
    }

    public void setItemClickListener(RecyclerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onItemClicked(v, (Integer) v.getTag());
    }

    @Override
    public boolean onLongClick(View v) {
        itemClickListener.onItemLongClicked(v, (Integer) v.getTag());
        return false;
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        TextView lblCityIcon;
        TextView lblCityName;
        LinearLayout layoutCity;

        public CityViewHolder(View itemView) {
            super(itemView);
            lblCityIcon = (TextView) itemView.findViewById(R.id.lbl_city);
            lblCityName = (TextView) itemView.findViewById(R.id.lbl_city_name);
            layoutCity = (LinearLayout) itemView.findViewById(R.id.city_parent);

            lblCityIcon.setTypeface(FontManager.getTypeface(itemView.getContext(), FontManager.FONT_MATERIAL));
        }
    }

    class AddCityViewHolder extends RecyclerView.ViewHolder {
        private final TextView lblAddIcon;
        RelativeLayout layoutAdd;


        public AddCityViewHolder(View itemView) {
            super(itemView);
            layoutAdd = (RelativeLayout) itemView.findViewById(R.id.add_parent);
            lblAddIcon = (TextView) itemView.findViewById(R.id.lbl_add_icon);
            lblAddIcon.setTypeface(FontManager.getTypeface(itemView.getContext(), FontManager.FONT_MATERIAL));
        }
    }
}
