package com.weatherapp.web;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.google.gson.Gson;
import com.weatherapp.database.models.Location;
import com.weatherapp.database.models.LocationWeather;
import com.weatherapp.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WeatherUtil implements LoaderManager.LoaderCallbacks<String> {
    private final FragmentActivity activity;
    private final WeatherUpdateListener weatherUpdateListener;
    private boolean isFutureForecast = false;

    public WeatherUtil(FragmentActivity activity, WeatherUpdateListener weatherUpdateListener) {
        this.weatherUpdateListener = weatherUpdateListener;
        this.activity = activity;
    }

    public boolean isFutureForecast() {
        return isFutureForecast;
    }

    public void setFutureForecast(boolean futureForecast) {
        isFutureForecast = futureForecast;
    }

    public void fetchWeatherForLocation(Location location) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("lat=" + location.getLatitude());
        stringBuilder.append("&lon=" + location.getLongitude());
        stringBuilder.append("&appid=" + Constants.API_KEY);
        stringBuilder.append("&units=" + Constants.UNIT);
        String params = stringBuilder.toString();
        String url;
        if (isFutureForecast) {
            url = Constants.WS_GET_FUTURE_WEATHER;
        } else {
            url = Constants.WS_GET_WEATHER;
        }

        Bundle bundle = new Bundle();
        bundle.putString("params", params);
        bundle.putString("url", url);

        LoaderManager supportLoaderManager = activity.getSupportLoaderManager();
        if (supportLoaderManager.getLoader(0) == null) {
            supportLoaderManager.initLoader(0, bundle, this).forceLoad();
        } else {
            supportLoaderManager.restartLoader(0, bundle, this).forceLoad();
        }
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new GetDataLoader(activity, args.getString("params"), args.getString("url"));
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        if (data != null) {
            Gson gson = new Gson();
            if (isFutureForecast) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray jsonArray = jsonObject.getJSONArray("list");
                    if (jsonArray != null) {
                        LocationWeather[] locationWeatherArr = gson.fromJson(String.valueOf(jsonArray), LocationWeather[].class);
                        List<LocationWeather> locationWeathers = new ArrayList<>();
                        locationWeathers.addAll(Arrays.asList(locationWeatherArr));
                        weatherUpdateListener.onFutureWeatherFetched(locationWeathers);
                    } else {
                        weatherUpdateListener.onWeatherFailed(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                LocationWeather locationWeather = gson.fromJson(data, LocationWeather.class);
                if (locationWeather.getWeather() == null) {
                    weatherUpdateListener.onWeatherFailed(locationWeather.getMessage());
                } else {
                    weatherUpdateListener.onWeatherFetched(locationWeather);
                }
            }
        } else {
            // IO exception.
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    public interface WeatherUpdateListener {
        void onWeatherFetched(LocationWeather locationWeather);

        void onFutureWeatherFetched(List<LocationWeather> locationWeathers);

        void onWeatherFailed(String message);
    }
}
