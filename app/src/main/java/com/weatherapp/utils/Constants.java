package com.weatherapp.utils;

public class Constants {
    public static final String DB_NAME = "db_weather";
    public static final int DB_VERSION = 1;

    public static final String BASE_URL = "http://api.openweathermap.org/";
    public static final String WS_GET_WEATHER = BASE_URL + "data/2.5/weather?";
    public static final String WS_GET_FUTURE_WEATHER = BASE_URL + "data/2.5/forecast?";

    public static final String API_KEY = "c6e381d8c7ff98f0fee43775817cf6ad";
    public static final String UNIT = "metric";

    public static final String SP_NAME = "sp_weather";
    public static final String SP_UNIT = "sp_unit";
    public static final String SP_FORECAST_LIMIT = "sp_limit";
}
