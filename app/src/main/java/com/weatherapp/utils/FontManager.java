package com.weatherapp.utils;

import android.content.Context;
import android.graphics.Typeface;

public class FontManager {
 
    public static final String ROOT = "fonts/",
    FONT_MATERIAL = ROOT + "MaterialIcons-Regular.ttf";

    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }    
 
}