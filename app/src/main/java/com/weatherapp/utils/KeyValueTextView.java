package com.weatherapp.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weatherapp.R;

public class KeyValueTextView extends LinearLayout {
    public KeyValueTextView(Context context) {
        super(context);
        setOrientation(VERTICAL);
        init(null);
    }

    public KeyValueTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public KeyValueTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.KeyValueTextView);

        try {
            String key = typedArray.getString(R.styleable.KeyValueTextView_key);
            String value = typedArray.getString(R.styleable.KeyValueTextView_value);

            addChildView("key", key);
            addChildView("value", value);
        } finally {
            typedArray.recycle();
        }

    }

    private void addChildView(String mode, String str) {
        if (str != null && str.length() > 0) {
            TextView inflatedView;
            if (mode.equals("key")) {
                inflatedView = (TextView) inflate(getContext(), R.layout.item_key, null);
            } else {
                inflatedView = (TextView) inflate(getContext(), R.layout.item_value, null);
            }
            inflatedView.setText(str);
            addView(inflatedView);
        }
    }

    public void setValue(String value) {
        addChildView("value", value);
        invalidate();
    }

    public void setKey(String key) {
        addChildView("key", key);
        invalidate();
    }
}
